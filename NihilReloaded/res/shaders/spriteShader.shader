#shader vertex
#version 330 core

layout(location = 0) in vec2 aPos;

out vec2 texCoord;

void main()
{
	gl_Position = vec4(aPos, 0.0, 1.0);
	texCoord = aPos;
}

#shader fragment
#version 330 core
out vec4 FragColor;

in vec2 texCoord;
uniform sampler2D ourTexture;

void main()
{
	FragColor =	texture(ourTexture, texCoord);
}