#shader vertex
#version 330 core

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec2 aTexCoords;
layout(location = 3) in float aTexID;

out vec2 texCoord;
out vec4 color;

void main()
{
	gl_Position = vec4(aPos, 0);
	texCoord = aTexCoords;
	color = aColor;
}

#shader fragment
#version 330 core

out vec4 FragColor;

in vec2 texCoord;
in vec4 color;

void main()
{
	FragColor = texture(ourTexture, texCoord) * color;
}