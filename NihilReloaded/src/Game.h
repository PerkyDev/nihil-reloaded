#pragma once
#include "graphics/Window.h"

namespace Nihil
{
	enum GameExitCode
	{
		GAME_SUCCESS = 1,
		GAME_FAILED = -1,
		GAME_GL_FAILED = -2
	};


	class Game
	{
	public:
		Game(Graphics::Window* window);
		GameExitCode Run(double tickRate);
		bool getRunning();


		~Game();

	private:
		Graphics::Window* m_Window;
		bool m_Running;
		void Update(double deltaTime);
		void Render();
		void Initialize();

	};

}