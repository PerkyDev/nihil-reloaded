#include "Window.h"

namespace Nihil
{
	namespace Graphics
	{
		Window::Window(const int width, const int height, const char* title)
			:m_Width(width), m_Height(height)
		{
			if (!glfwInit())
				return;

			glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

			m_Window = glfwCreateWindow(width, height, title, NULL, NULL);			


			if (m_Window == NULL)
				return;
			
			glfwMakeContextCurrent(m_Window);

			m_Initialized = true;
		}

		Window::~Window()
		{
			glfwDestroyWindow(m_Window);
		}

		void Window::Show() const
		{
			glfwSwapBuffers(m_Window);
		}

		bool Window::getShouldClose() const
		{
			return glfwWindowShouldClose(m_Window);
		}

		bool Window::getInitialized() const
		{
			return m_Initialized;
		}


	}
}