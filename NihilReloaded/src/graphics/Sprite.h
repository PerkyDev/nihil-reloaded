#pragma once
#include "GL/glew.h"
#include "Texture.h"
#include "Rectangle.h"
#include "resources/Resource.h"


namespace Nihil
{
	namespace Graphics
	{
		struct Sprite
		{
		public:
			Sprite();

			inline void setTexture(std::shared_ptr<Resources::Resource<Texture>> textureResource)
			{
				m_TextureResource = textureResource;
			}
			
			inline const Texture& getTexture() const
			{
				return *m_TextureResource->getData();
			}

			inline const Rectanglef& getSourceRectangle() const
			{
				return m_SourceRectangle;
			}

			void SetSourceRectangle(const Rectangle& rectangle);

		private:
			Rectanglef m_SourceRectangle;
			std::shared_ptr<Resources::Resource<Texture>> m_TextureResource;
		};
	}
}