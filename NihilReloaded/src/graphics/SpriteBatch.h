#pragma once
#include <GL/glew.h>
#include "Sprite.h"
#include "Shader.h"

namespace Nihil
{
	namespace Graphics
	{
		struct Vec2
		{
			GLfloat x, y;
		};

		struct Vec3
		{
			GLfloat x, y, z;
		};

		struct Vec4
		{
			GLfloat x, y, z, w;
		};

		class SpriteBatch
		{
		public:
			struct Vertex
			{
				Vec3 Position;
				Vec4 Color;
				Vec2 TexCoords;
				GLfloat TexID;
			};

			SpriteBatch(GLuint spriteCount); 
			void Initialize();
			~SpriteBatch();

			void Begin(std::shared_ptr<Resources::Resource<Shader>> shader);
			void Draw(const Sprite& sprite, const Vec2& position, const Vec4& color, const GLfloat& depth);
			void End();

		private:
			void Flush();
			std::shared_ptr<Resources::Resource<Shader>> m_Shader;

			const GLint m_BufferSize;
			const GLint m_MaxSprites;
			GLuint m_VertexBuffer, m_ElementBuffer, m_VertexArray;
			GLuint m_BufferIndex;


		};

	}
}