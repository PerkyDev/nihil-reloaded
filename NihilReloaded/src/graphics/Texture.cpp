#include "Texture.h"


namespace Nihil
{
	namespace Graphics
	{

		Texture::Texture(const Bitmap* bitmap)
			: m_Width(bitmap->getWidth()), m_Height(bitmap->getHeight())
		{
			glGenTextures(1, &m_TextureHandle);
			glBindTexture(GL_TEXTURE_2D, m_TextureHandle);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			const GLubyte* data = bitmap->getData();

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_Width, m_Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap->getData());

			glBindTexture(GL_TEXTURE_2D, 0);
		}


		Texture::~Texture()
		{
			glDeleteTextures(1, &m_TextureHandle);
		}

	}

}