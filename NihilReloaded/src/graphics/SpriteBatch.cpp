#include "SpriteBatch.h"
#include <stddef.h>
#include <iostream>

namespace Nihil
{
	namespace Graphics
	{
#define SPRITE_BATCH_VERTEXLAYOUT_POSITION	0
#define SPRITE_BATCH_VERTEXLAYOUT_COLOR		1
#define SPRITE_BATCH_VERTEXLAYOUT_TEXCOORDS 2
#define SPRITE_BATCH_VERTEXLAYOUT_TEXID		3

		SpriteBatch::SpriteBatch(GLuint spriteCount)
			:m_BufferSize(spriteCount * 4), m_MaxSprites(spriteCount), m_BufferIndex(0)
		{

		}

		void SpriteBatch::Initialize()
		{

			//Create buffers
			glGenBuffers(1, &m_VertexBuffer);
			glGenBuffers(1, &m_ElementBuffer);

			//Create and bind VertexArray
			glGenVertexArrays(1, &m_VertexArray);
			glBindVertexArray(m_VertexArray);

			//Setup VertexBuffer
			glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_BufferSize, nullptr, GL_DYNAMIC_DRAW);

			//Define Vertexlayout
			glEnableVertexAttribArray(SPRITE_BATCH_VERTEXLAYOUT_POSITION);
			glEnableVertexAttribArray(SPRITE_BATCH_VERTEXLAYOUT_COLOR);
			glEnableVertexAttribArray(SPRITE_BATCH_VERTEXLAYOUT_TEXCOORDS);
			glEnableVertexAttribArray(SPRITE_BATCH_VERTEXLAYOUT_TEXID);

			glVertexAttribPointer(SPRITE_BATCH_VERTEXLAYOUT_POSITION,	sizeof(GLfloat) * 3,
				GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position));

			glVertexAttribPointer(SPRITE_BATCH_VERTEXLAYOUT_COLOR,		sizeof(GLfloat) * 4,
				GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color));

			glVertexAttribPointer(SPRITE_BATCH_VERTEXLAYOUT_TEXCOORDS,	sizeof(GLfloat) * 2,
				GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, TexCoords));

			glVertexAttribPointer(SPRITE_BATCH_VERTEXLAYOUT_TEXID,		sizeof(GLfloat) * 1,
				GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, TexID));

			//Setup element buffer
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ElementBuffer);

			GLuint* elements = new GLuint[m_MaxSprites * 6];

			for (int i = 0; i < m_MaxSprites * 6 - 6; i+=6)
			{
				elements[i + 0] = i + 0; //Top left
				elements[i + 1] = i + 2; //Bottom left
				elements[i + 2] = i + 1; //Top right
				elements[i + 3] = i + 1; //Top Right
				elements[i + 4] = i + 2; //Bottom left
				elements[i + 6] = i + 3; //Bottom right
			}

			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * m_MaxSprites * 6, (const void*)elements, GL_STATIC_DRAW);

			//Cleanup
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
			delete elements;
		}

		SpriteBatch::~SpriteBatch()
		{
			glDeleteBuffers(1, &m_VertexBuffer);
			glDeleteBuffers(1, &m_ElementBuffer);
			glDeleteVertexArrays(1, &m_VertexArray);

		}

		void SpriteBatch::Begin(std::shared_ptr<Resources::Resource<Shader>> shader)
		{
			m_Shader = shader;
			shader->getData()->BindProgram();

			glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);

		}

		void SpriteBatch::Draw(const Sprite& sprite, const Vec2& position, const Vec4& color, const GLfloat& depth)
		{
			Vertex vertices[4];
			const Rectanglef& srcRectangle = sprite.getSourceRectangle();

			vertices[0].Position = { position.x + 0, position.y + 0, depth };
			vertices[1].Position = { position.x + 1, position.y + 0, depth };
			vertices[2].Position = { position.x + 0, position.y + 1, depth };
			vertices[3].Position = { position.x + 1, position.y + 1, depth };
			
			vertices[0].TexCoords = { srcRectangle.x, srcRectangle.y};
			vertices[1].TexCoords = { srcRectangle.x + srcRectangle.width, srcRectangle.y};
			vertices[2].TexCoords = { srcRectangle.x, srcRectangle.y + srcRectangle.height};
			vertices[3].TexCoords = { srcRectangle.x + srcRectangle.width, srcRectangle.y + srcRectangle.height};

			for (int i = 0; i < 4; i++)
			{
				vertices[i].Color = color;
			}

			glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_BufferIndex, sizeof(Vertex) * 4, vertices);
			
			m_BufferIndex += 4;
			if (m_BufferIndex == m_BufferSize)
			{
				Flush();
			}
		}

		void SpriteBatch::Flush()
		{
			glBindVertexArray(m_VertexArray);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ElementBuffer);

			glDrawElements(GL_TRIANGLES, m_MaxSprites * 6, GL_UNSIGNED_INT, nullptr);
			m_BufferIndex = 0;

			glBindVertexArray(0);
		}

		void SpriteBatch::End()
		{
			Flush();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			m_Shader->getData()->UnbindProgram();
		}

	}
}