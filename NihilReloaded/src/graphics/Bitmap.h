#pragma once
#include "GL/glew.h"

namespace Nihil
{
	namespace Graphics
	{
		struct Bitmap
		{
		public:
			Bitmap(const char* imageFilePath);
			Bitmap(GLubyte* data, const GLuint& width, const GLuint& height);
			~Bitmap();

			inline const GLubyte* getData() const
			{
				return m_Pixels;
			}

			inline GLuint getWidth() const
			{
				return m_Width;
			}

			inline GLuint getHeight() const
			{
				return m_Height;
			}

			inline GLuint getNrChannels() const
			{
				return m_NrChannels;
			}

		private:
			GLubyte* m_Pixels = nullptr;
			GLuint m_Width = 0, m_Height = 0;
			GLuint m_NrChannels = 0;
			
		};


	}
}