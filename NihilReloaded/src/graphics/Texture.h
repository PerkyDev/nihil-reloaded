#pragma once
#include "GL/glew.h"
#include "stb_image.h"
#include "Bitmap.h"

#include <string>
#include <vector>

namespace Nihil
{
	namespace Graphics
	{
		class Texture
		{

		public:
			Texture(const Bitmap* bitmap);

			inline GLuint getWidth() const
			{
				return m_Width;
			}

			inline GLuint getHeight() const
			{
				return m_Height;
			}

			inline GLuint getHandle() const
			{
				return m_TextureHandle;
			}

			~Texture();



		private:
			GLuint m_TextureHandle;
			const GLuint m_Width, m_Height;

		};
	}
}