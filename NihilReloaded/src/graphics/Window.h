#pragma once
#include "GLFW/glfw3.h"

namespace Nihil
{
	namespace Graphics
	{

		class Window
		{
		
		public:
			Window(const int width, const int height, const char* title);
			bool getInitialized() const;
			bool getShouldClose() const;

			void Show() const;
			~Window();

		private:
			GLFWwindow*	m_Window = NULL;
			
			int		m_Width, m_Height;
			bool	m_Initialized;


		};

	}
}
