#include "Sprite.h"

namespace Nihil
{
	namespace Graphics
	{

		Sprite::Sprite()
			:m_SourceRectangle(0, 0, 0, 0)
		{

		}
		void Sprite::SetSourceRectangle(const Rectangle& rectangle)
		{
			GLint texWidth = m_TextureResource->getData()->getWidth();
			GLint texHeight = m_TextureResource->getData()->getHeight();

			m_SourceRectangle.width	=	(float)rectangle.width	/	(float)texWidth;
			m_SourceRectangle.height =	(float)rectangle.height /	(float)texHeight;
			m_SourceRectangle.x =		(float)rectangle.x		/	(float)texWidth;
			m_SourceRectangle.y =		(float)rectangle.y		/	(float)texHeight;
		}

	}
}