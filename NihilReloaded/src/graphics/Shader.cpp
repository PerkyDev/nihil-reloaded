#include "Shader.h"
#include <sstream>
#include <fstream>
#include <iostream>

namespace Nihil
{
	namespace Graphics
	{
		Shader::Shader(const char* vertexSource, const char* fragmentSource)
		{

			m_ProgramHandle = glCreateProgram();
			GLuint vertexShader = CompileShader(GL_VERTEX_SHADER, vertexSource);
			GLuint fragmentShader = CompileShader(GL_FRAGMENT_SHADER, fragmentSource);

			glAttachShader(m_ProgramHandle, vertexShader);
			glAttachShader(m_ProgramHandle, fragmentShader);
			glLinkProgram(m_ProgramHandle);
			glValidateProgram(m_ProgramHandle);

			GLint result;
			glGetProgramiv(m_ProgramHandle, GL_LINK_STATUS, &result);

			if (result == GL_FALSE)
			{
				//std::cout << "Program failed linking!" << std::endl;
				int length;
				glGetProgramiv(m_ProgramHandle, GL_INFO_LOG_LENGTH, &length);

				char* message = new char[length];
				glGetProgramInfoLog(m_ProgramHandle, length, &length, message);
				std::cout << message << std::endl;
				delete message;

			}

			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);
		}

		Shader::~Shader()
		{
			glDeleteProgram(m_ProgramHandle);
		}

		const void Shader::BindProgram() const
		{
			glUseProgram(m_ProgramHandle);
		}

		const void Shader::UnbindProgram() const
		{
			glUseProgram(0);
		}

		unsigned int Shader::CompileShader(unsigned int type, const char* source)
		{
			GLuint id = glCreateShader(type);
			glShaderSource(id, 1, &source, nullptr);
			glCompileShader(id);

			int result;
			glGetShaderiv(id, GL_COMPILE_STATUS, &result);
			if (result == GL_FALSE)
			{
				int length;
				glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
				char* message = new char[length];
				glGetShaderInfoLog(id, length, &length, message);

				switch (type)
				{

				case GL_VERTEX_SHADER:
					std::cout << "Vertex shader failed to compile!" << std::endl;
					break;
				case GL_FRAGMENT_SHADER:
					std::cout << "Fragment shader failed to compile!" << std::endl;
					break;
				}

				std::cout << source << std::endl;
				std::cout << message << std::endl;


				delete message;

			}

			return id;

		}

		const Shader* Shader::ParseShader(const std::string& filePath)
		{
			std::ifstream stream(filePath);

			enum class ShaderType
			{
				NONE = -1, VERTEX = 0, FRAGMENT = 1
			};

			ShaderType type = ShaderType::NONE;

			std::string line;
			std::stringstream ss[2];

			while (getline(stream, line))
			{

				if (line.find("#shader") != std::string::npos)
				{
					if (line.find("vertex") != std::string::npos)
					{
						type = ShaderType::VERTEX;
					}
					else if(line.find("fragment") != std::string::npos)
					{
						type = ShaderType::FRAGMENT;
					}
				}
				else
				{
					ss[(int)type] << line << '\n';

				}
			}

			std::string vertexSourceString = ss[(int)ShaderType::VERTEX].str();
			std::string fragmentSourceString = ss[(int)ShaderType::FRAGMENT].str();

			const char* vertexSource = vertexSourceString.c_str();
			const char* fragmentSource = fragmentSourceString.c_str();

			return new Shader(vertexSource, fragmentSource);

		}


	}
}