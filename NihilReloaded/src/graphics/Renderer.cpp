#include "Renderer.h"
#include "Texture.h"

namespace Nihil
{
	namespace Graphics
	{
		GLuint Renderer::s_SpriteVAO;
		GLuint Renderer::s_SpriteVBO;
		GLuint Renderer::s_SpriteEBO;

		std::shared_ptr<Resources::Resource<Shader>> Renderer::s_SpriteShader;

		bool Renderer::Initialize()
		{
			if (glewInit() != GLEW_OK)
				return false;

			InitializeSpriteRendering();
			return true;
		}

		void Renderer::InitializeSpriteRendering()
		{
			GLfloat rectangleVertices[8]
			{
				0, 0, //top left - 0
				0, 1, //bottom left - 1
				1, 0, //top right - 2 
				1, 1, //bottom right - 3
			};

			GLuint elementArray[6]
			{
				2,
				0,
				1,
				2,
				1,
				3
			};

			glGenBuffers(1, &s_SpriteVBO);
			glGenBuffers(1, &s_SpriteEBO);

			glGenVertexArrays(1, &s_SpriteVAO);
			glBindVertexArray(s_SpriteVAO);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_SpriteEBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementArray), elementArray, GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, s_SpriteVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, 0);
			glEnableVertexAttribArray(0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

		}

		const void Renderer::BeginDrawSprite()
		{
			s_SpriteShader->getData()->BindProgram();
			glBindVertexArray(s_SpriteVAO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_SpriteEBO);
		}

		const void Renderer::DrawSprite(const Sprite& sprite)
		{
			GLint texHandle = sprite.getTexture().getHandle();
			glBindTexture(GL_TEXTURE_2D, sprite.getTexture().getHandle());
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		}

		const void Renderer::EndDrawSprite()
		{
			glUseProgram(0);
		}

		void Renderer::SetSpriteShader(std::shared_ptr<Resources::Resource<Shader>> spriteShader)
		{
			s_SpriteShader = spriteShader;
		}

		void Renderer::CleanUp()
		{

		}

		
	}
}