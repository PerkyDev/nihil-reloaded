#pragma once
#include <GL/glew.h>
#include <string>

namespace Nihil
{
	namespace Graphics
	{
		class Shader
		{
		public:
			Shader(const char* vertexSource, const char* fragmentSource);

			static unsigned int CompileShader(unsigned int type, const char* source);
			static const Shader* ParseShader(const std::string& filePath);
			const void BindProgram() const;
			const void UnbindProgram() const;

			~Shader();

		private:
			GLuint m_ProgramHandle;
		
		};


	}
}