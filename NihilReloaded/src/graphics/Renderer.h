#pragma once
#include "GL/glew.h"
#include "Shader.h"
#include "resources/Resource.h"
#include "Sprite.h"

namespace Nihil
{
	namespace Graphics
	{
		class Renderer
		{
		public:
			static bool Initialize();

			static void CleanUp();
			static const void BeginDrawSprite();
			static const void EndDrawSprite();
			static const void DrawSprite(const Sprite& sprite);
			static void SetSpriteShader(std::shared_ptr<Resources::Resource<Shader>> spriteShader);

		private:
			static GLuint s_SpriteVBO;
			static GLuint s_SpriteEBO;
			static GLuint s_SpriteVAO;
			static std::shared_ptr<Resources::Resource<Shader>> s_SpriteShader;
			static void InitializeSpriteRendering();


		};

	}
}