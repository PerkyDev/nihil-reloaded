#include "Bitmap.h"
#include "stb_image.h"
#include <iostream>

namespace Nihil
{
	namespace Graphics
	{
		Bitmap::Bitmap(const char* imageFilePath)
		{
			int width, height;
			int nrChannels;
			m_Pixels = stbi_load(imageFilePath, &width, &height, &nrChannels, 4);

			if (m_Pixels != NULL)
			{
				m_Width = width;
				m_Height = height;
				m_NrChannels = nrChannels;
			}
			else
			{
				std::cout << stbi_failure_reason() << std::endl;
			}
			
		}

		Bitmap::Bitmap(GLubyte* data, const GLuint& width, const GLuint& height)
			: m_Width(width), m_Height(height)
		{
			m_Pixels = data;
			m_NrChannels = 4;
		}


		Bitmap::~Bitmap()
		{
			stbi_image_free(m_Pixels);
		}

	}
}