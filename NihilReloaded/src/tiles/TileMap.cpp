#include "TileMap.h"

namespace Nihil
{
	namespace Tiles
	{

		TileMap::TileMap(GLuint width, GLuint height, std::shared_ptr<Resource<Texture>> texture)
			:m_Texture(texture), m_Width(width), m_Height(height)
		{

		}

		void TileMap::CreateBuffers()
		{
			int size = m_Width * m_Height;
			m_Vertices = new GLfloat[m_Width * m_Height * 2];

			for (int i = 0; i < m_Width; i+=2)
			{
				for (int j = 0; j < m_Height; j+=2)
				{
					int index = i + j * m_Width;
					m_Vertices[index] = i;
					if (index < size - 1)
						m_Vertices[index + 1] = j;
				}
			}


		}

		TileMap::~TileMap()
		{
			delete[] m_Vertices;
		}
	}
}