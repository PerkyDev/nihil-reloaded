#pragma once
#include "resources/Resource.h"
#include "graphics/Texture.h"
namespace Nihil
{
	using namespace Graphics;
	using namespace Resources;
	namespace Tiles
	{
		class TileMap
		{
		public:
			TileMap(const GLuint width, const GLuint height, std::shared_ptr<Resource<Texture>> texture);
			void CreateBuffers();

			~TileMap();

		private:
			std::shared_ptr<Resource<Texture>> m_Texture;
			const GLuint m_Width, const m_Height;
			GLfloat* m_Vertices;

			GLint m_VBO, m_EBO, m_VAO;
		};
	}
}