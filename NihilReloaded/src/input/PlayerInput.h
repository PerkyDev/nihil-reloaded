#pragma once
#include <GLFW/glfw3.h>
#include <unordered_map>
#include <string>

namespace Nihil
{
	namespace Input
	{
		enum PlayerInputMode
		{
			PLAYER_INPUT_MODE_CONTROLLER,
			PLAYER_INPUT_MODE_KEYBOARD,
			PLAYER_INPUT_MODE_NONE
		};

		enum ControllerConnectionState
		{
			CONTROLLER_CONNECTION_NONE,
			CONTROLLER_CONNECTION_CONNECTED,
			CONTROLLER_CONNECTION_DISCONNECTED,

		};

		enum GamePadButton
		{
			GAMEPAD_BUTTON_A = GLFW_GAMEPAD_BUTTON_A,
			GAMEPAD_BUTTON_B = GLFW_GAMEPAD_BUTTON_B,
			GAMEPAD_BUTTON_X = GLFW_GAMEPAD_BUTTON_X,
			GAMEPAD_BUTTON_Y = GLFW_GAMEPAD_BUTTON_Y,
			GAMEPAD_BUTTON_DPAD_LEFT = GLFW_GAMEPAD_BUTTON_DPAD_LEFT,
			GAMEPAD_BUTTON_DPAD_RIGHT = GLFW_GAMEPAD_BUTTON_DPAD_RIGHT,
			GAMEPAD_BUTTON_DPAD_UP = GLFW_GAMEPAD_BUTTON_DPAD_UP,
			GAMEPAD_BUTTON_DPAD_DOWN = GLFW_GAMEPAD_BUTTON_DPAD_DOWN,
			GAMEPAD_BUTTON_LB = GLFW_GAMEPAD_BUTTON_LEFT_BUMPER,
			GAMEPAD_BUTTON_RB = GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER,
			GAMEPAD_BUTTON_START = GLFW_GAMEPAD_BUTTON_START,
			GAMEPAD_BUTTON_BACK = GLFW_GAMEPAD_BUTTON_BACK
		};

		enum GamePadAxis
		{
			GAMEPAD_AXIS_LEFT_X = GLFW_GAMEPAD_AXIS_LEFT_X,
			GAMEPAD_AXIS_LEFT_Y = GLFW_GAMEPAD_AXIS_LEFT_Y,
			GAMEPAD_AXIS_RIGHT_X = GLFW_GAMEPAD_AXIS_RIGHT_X,
			GAMEPAD_AXIS_RIGHT_Y = GLFW_GAMEPAD_AXIS_RIGHT_Y,
			GAMEPAD_AXIS_LEFT_TRIGGER = GLFW_GAMEPAD_AXIS_LEFT_TRIGGER,
			GAMEPAD_AXIS_RIGHT_TRIGGER = GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER,
		};

		enum PlayerIndex
		{
			PLAYER_ONE,
			PLAYER_TWO,
			PLAYER_THREE,
			PLAYER_FOUR
		};

		class InputSystem;

		struct GamePadState
		{
			friend class PlayerInput;
		public: 
			void setGLFWGamePadState(GLFWgamepadstate state);
			GLFWgamepadstate* getGLFWGamePadStatePointer();
			const GLFWgamepadstate& getGLFWGamePadState();

			const inline bool IsButtonDown(GamePadButton button) const
			{
				return m_State.buttons[button];
			}

			const inline float GetAxis(GamePadAxis axis) const
			{
				return m_State.axes[axis];
			}

		private:
			GLFWgamepadstate m_State;
		};

		class PlayerInput
		{
			friend class InputSystem;
		public:
			const inline GamePadState getGamePadState() const
			{
				return m_GamePadState;
			}

			const inline PlayerInputMode getInputMode() const
			{
				return m_InputMode;
			}

			const inline ControllerConnectionState getControllerConnectionStatus() const
			{
				return m_ControllerConnectionState;
			}

			const bool ButtonPressed(GamePadButton button) const;
			const bool ButtonDown(GamePadButton button) const;
			const bool ButtonReleased(GamePadButton button) const;

			const float GetAxis(GamePadAxis axis) const;

		private:
			GamePadState m_GamePadState;
			GamePadState m_OldGamePadState;

			PlayerInputMode m_InputMode = PLAYER_INPUT_MODE_NONE;
			ControllerConnectionState m_ControllerConnectionState = CONTROLLER_CONNECTION_NONE;

		};

		class InputSystem
		{
		private:
			static PlayerInput m_PlayerInputs[4];

		public:
			static void Update();
			static bool DisconnectedControllers[4];
			static PlayerInput* getInput(PlayerIndex index);
			static void keyboard_input_callback_fn(GLFWwindow* window, int key, int scancode, int action, int mods);

		};

	}
}