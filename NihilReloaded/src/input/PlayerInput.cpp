#include "PlayerInput.h"
#include <iostream>

namespace Nihil
{
	namespace Input
	{

		PlayerInput InputSystem::m_PlayerInputs[4];
		bool InputSystem::DisconnectedControllers[4];

		void GamePadState::setGLFWGamePadState(GLFWgamepadstate state)
		{
			m_State = state;
		}

		const GLFWgamepadstate& GamePadState::getGLFWGamePadState()
		{
			return m_State;
		}

		GLFWgamepadstate* GamePadState::getGLFWGamePadStatePointer()
		{
			return &m_State;
		}

		void InputSystem::keyboard_input_callback_fn(GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			for (int i = 0; i < 4; i++)
			{
				PlayerInput& input = m_PlayerInputs[i];
				if (input.m_InputMode != PLAYER_INPUT_MODE_KEYBOARD)
					continue;


				break;

			}
		}


		void InputSystem::Update()
		{
			//ASSIGN CONTROLLERS AND UPDATE CONTROLLERS
			for (int i = 0; i < 4; i++)
			{
				PlayerInput& input = m_PlayerInputs[i];

				input.m_OldGamePadState.setGLFWGamePadState(input.m_GamePadState.getGLFWGamePadState());

				if (!glfwGetGamepadState(i, input.m_GamePadState.getGLFWGamePadStatePointer()))
				{
					if (input.m_InputMode == PLAYER_INPUT_MODE_KEYBOARD)
						break;

					if (DisconnectedControllers[i])
						continue;

					if (input.m_ControllerConnectionState == CONTROLLER_CONNECTION_CONNECTED)
					{
						input.m_ControllerConnectionState = CONTROLLER_CONNECTION_DISCONNECTED;
						DisconnectedControllers[i] = true;
						std::cout << "Controller disconnected! (Player " << i << ")" << std::endl;

					}
					else
					{
						input.m_InputMode = PLAYER_INPUT_MODE_KEYBOARD;
						input.m_ControllerConnectionState = CONTROLLER_CONNECTION_NONE;
						std::cout << "Assigned keyboard! (Player " << i << ")" << std::endl;
					}
					break;
				}
				else if(input.m_ControllerConnectionState == CONTROLLER_CONNECTION_NONE)
				{
					input.m_ControllerConnectionState = CONTROLLER_CONNECTION_CONNECTED;
					input.m_InputMode = PLAYER_INPUT_MODE_CONTROLLER;
					std::cout << "Assigned gamepad! (Player " << i << ")"<<std::endl;
				}

			}

			//Try to reconnect disconnected controllers
			for (int i = 0; i < 4; i++)
			{
				if (DisconnectedControllers[i])
				{
					PlayerInput& input = m_PlayerInputs[i];

					if (glfwGetGamepadState(i, input.m_GamePadState.getGLFWGamePadStatePointer()))
					{
						input.m_ControllerConnectionState = CONTROLLER_CONNECTION_CONNECTED;
						input.m_InputMode = PLAYER_INPUT_MODE_CONTROLLER;

						std::cout << "Controller reconnected! (Player " << i << ")" << std::endl;
						DisconnectedControllers[i] = false;
						
						for (int j = 0; j < 4; j++)
						{
							std::cout << "Input mode (Player " << j << "): ";
							switch (m_PlayerInputs[j].m_InputMode)
							{
							case PLAYER_INPUT_MODE_KEYBOARD:
								std::cout << "Keyboard";
								break;
							case PLAYER_INPUT_MODE_CONTROLLER:
								std::cout << "Gamepad";
								break;
							case PLAYER_INPUT_MODE_NONE:
								std::cout << "None";
								break;
							}
							std::cout << std::endl;
						}
					}

				}
			}
		}

		PlayerInput* InputSystem::getInput(PlayerIndex index)
		{
			return &m_PlayerInputs[index];
		}

		const bool PlayerInput::ButtonPressed(GamePadButton button) const
		{
			return m_GamePadState.m_State.buttons[button] && !m_OldGamePadState.m_State.buttons[button];
		}

		const bool PlayerInput::ButtonDown(GamePadButton button) const
		{
			return m_GamePadState.m_State.buttons[button];
		}

		const bool PlayerInput::ButtonReleased(GamePadButton button) const
		{
			return !m_GamePadState.m_State.buttons[button] && m_OldGamePadState.m_State.buttons[button];

		}

		const float PlayerInput::GetAxis(GamePadAxis axis) const
		{
			return m_GamePadState.m_State.axes[axis];
		}



	

	}
}