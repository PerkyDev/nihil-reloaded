#include "graphics/Bitmap.h"
#include "resources/Resource.h"
#include "graphics/Texture.h"
#include "graphics/Renderer.h"
#include "graphics/SpriteBatch.h"
#include "graphics/Sprite.h"

#include "input/PlayerInput.h"
#include "Game.h"

#include <iostream>


namespace Nihil
{
	using namespace Resources;
	using namespace Graphics;
	using namespace Input;

	double FPS = 0;

	Sprite sprite;
	SpriteBatch spriteBatch(1000);
	
	PlayerInput* input;

	Game::Game(Graphics::Window* window)
		:m_Window(window)
	{
		glClearColor(0, 0.6f, 0.8f, 1.0f);
	}

	Game::~Game()
	{
		delete m_Window;
	}

	bool Game::getRunning()
	{
		return m_Running;
	}

	GameExitCode Game::Run(double tickRate)
	{
		if (!m_Window->getInitialized())
			return GAME_FAILED;

		std::cout << tickRate << std::endl;
		Initialize();

		m_Running = true;

		double lastTime = 0;
		double timeSinceLastUpdate = 0;
		double secondCounter = 0;

		int updatesCounter = 0;

		while (m_Running)
		{
			double currentTime = glfwGetTime();
			double deltaTime = currentTime - lastTime;

			timeSinceLastUpdate += deltaTime;
			secondCounter += deltaTime;

			if (timeSinceLastUpdate > tickRate)
			{
				glfwPollEvents();
				Nihil::Input::InputSystem::Update();

				Update(timeSinceLastUpdate);
				
				timeSinceLastUpdate -= tickRate;
				updatesCounter++;
			}

			if (secondCounter >= 1.0)
			{
				FPS = ((double)updatesCounter / secondCounter);

				secondCounter = 0;
				updatesCounter = 0;
			}

			if (m_Running == true)
				m_Running = !m_Window->getShouldClose();

			Render();

			lastTime = currentTime;
		}

		return GAME_SUCCESS;

	}


	void Game::Initialize()
	{
		spriteBatch.Initialize();

		Bitmap* bmp = new Graphics::Bitmap("res/textures/obliviouspp.png");
		
		Resource<Texture>::Create(new Texture(bmp), "testTex");
		Resource<Shader>::Create(Shader::ParseShader("res/shaders/spriteShader.shader"), "spriteShader");
		Resource<Shader>::Create(Shader::ParseShader("res/shaders/spriteBatchShader.shader"), "spriteBatchShader");

		Renderer::SetSpriteShader(Resource<Graphics::Shader>::Get("spriteShader"));
		sprite.setTexture(Resource<Texture>::Get("testTex"));
		
		input = InputSystem::getInput(PlayerIndex::PLAYER_ONE);

		sprite.SetSourceRectangle(Rectangle(0, 0, 32, 32));

		delete bmp;
	}

	void Game::Update(double deltaTime)
	{
		if (input->ButtonPressed(GAMEPAD_BUTTON_BACK))
		{
			std::cout << "Updates per second: " << FPS << std::endl;
		}
	}

	void Game::Render()
	{
		glClear(GL_COLOR_BUFFER_BIT);
		spriteBatch.Begin(Resource<Shader>::Get("spriteBatchShader"));
		spriteBatch.Draw(sprite, Vec2{ 0.0f, 0.0f }, Vec4{ 1.0f, 1.0f, 1.0f, 1.0f }, 0.0f);
		spriteBatch.End();
		/*
		Renderer::BeginDrawSprite();
		Renderer::DrawSprite(sprite);
		Renderer::EndDrawSprite();
		*/
		m_Window->Show();
	}



}