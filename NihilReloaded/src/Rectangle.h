#pragma once
namespace Nihil
{
	struct Rectanglef
	{
		Rectanglef(const float& x, const float& y, const float& width, const float& height);

		float x, y;
		float width, height;

		bool Intersecting(const Rectanglef& other) const;

	};

	struct Rectangle
	{
		Rectangle(const int& x, const int& y, const int& width, const int& height);

		int x, y;
		int width, height;

		bool Intersecting(const Rectangle& other) const;

	};

}