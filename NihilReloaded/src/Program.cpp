#include "graphics/Renderer.h"
#include "graphics/Window.h"
#include "Game.h"
#include <iostream>
int main()
{

	Nihil::Graphics::Window* window = new Nihil::Graphics::Window(1280, 720, "Nihil");
	Nihil::Game game(window);
	
	if (!Nihil::Graphics::Renderer::Initialize())
		return Nihil::GAME_GL_FAILED;

	Nihil::GameExitCode sucess = game.Run(1.0 / 60.0);
	Nihil::Graphics::Renderer::CleanUp();

	return sucess;
}