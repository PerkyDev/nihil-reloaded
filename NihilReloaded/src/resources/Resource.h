#pragma once
#include <string>
#include <stdlib.h>
#include <unordered_map>

namespace Nihil
{
	namespace Resources
	{
		template <typename T>
		class Resource
		{

		public:
			inline const T* getData() const
			{
				return m_Data;
			}

			inline const std::string& getName() const
			{
				return m_Name;
			}
			
			Resource(const T* data, const std::string& name)
				:m_Name(name), m_Data(data)
			{
			}

			static void Create(const T* data, const std::string& name)
			{
				s_LoadedResources[name] = std::make_shared<Resource<T>>(data, name);
			}

			static std::shared_ptr<Resource<T>> Get(const std::string& name)
			{
				return s_LoadedResources[name];
			}

			~Resource()
			{
				delete m_Data;
			}

		private:

			const std::string m_Name;
			const T* m_Data;
			static std::unordered_map<std::string, std::shared_ptr<Resource<T>>> s_LoadedResources;

		};

		template <typename T>
		std::unordered_map<std::string, std::shared_ptr<Resource<T>>> Resource<T>::s_LoadedResources;
		
	}
}

